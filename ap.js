if (!window.AP) {
    //throw new Error('AP is not defined. This component can be only used within an Atlassian Connect add-on.')
}
const AP = window.AP;

export default AP;