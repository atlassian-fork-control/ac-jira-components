module.exports = {
    entry: [
        './node_modules/webcomponents.js/CustomElements.min.js',
        './main.js'
    ],
    output: {
        filename: 'index.js'
    },
    devServer: {
        inline: true,
        port: 3333
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.css$/,
                loader: 'css?modules&camelCase!postcss-loader'
            }

        ]
    },
    postcss: function () {
        return [require('postcss-mixins'), require('autoprefixer')];
    }
};