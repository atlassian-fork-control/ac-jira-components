function renderSuggestions(element, suggestions, selectedIdx) {
    element.innerHTML = suggestions.map((s, idx) => {
        if (idx === selectedIdx) {
            return `<div style="color: #fff; background: dodgerblue; display: block;">${s.label}</div>`;
        } else {
            return `<div>${s.label}</div>`;
        }
    }).join('\n');
}

function getStyle(x, y, width) {
    if (x === undefined || y === undefined || width === undefined) {
        return `display: none;`;
    }

    return `
        font-family: sans-serif;
        line-height: 1.4;
        padding: 5px;
        box-sizing: border-box;
        position: fixed;
        top: ${y};
        right: ${x};
        width: ${width}px;
        background: white;
        box-shadow: 0 0 3px #999;
        border-bottom-right-radius: 3.01px;
        border-bottom-left-radius: 3.01px;
    `;
}

export function startSuggestions() {
    return new Promise((resolve, reject) => {
        let container = document.createElement('div');
        container.setAttribute('style', getStyle());
        document.body.appendChild(container);

        let currentSuggestions = [];
        let selectedIdx;
        let isShown = false;
        let onSelectionCallback = function() {};

        let lastX;
        let lastY;
        let lastWidth;

        let api = {
            setSuggestions(suggestions) {
                currentSuggestions = suggestions;
                selectedIdx = 0;
                console.log(`Suggestions set:`, currentSuggestions);
                if (isShown) {
                    renderSuggestions(container, currentSuggestions, selectedIdx);
                }
            },
            showAt(x, y, width) {
                isShown = true;
                console.log(`Showing at (${x},${y}) in ${width}px.`, currentSuggestions);
                renderSuggestions(container, currentSuggestions, selectedIdx);
                container.setAttribute('style', getStyle(x, y, width));
                lastX = x;
                lastY = y;
                lastWidth = width;
            },
            hide() {
                isShown = false;
                selectedIdx = 0;
                console.log('Suggestions hidden');
                container.innerHTML = '';
                container.setAttribute('style', getStyle());
            },
            moveDown() {
                if (isShown) {
                    selectedIdx = (selectedIdx + 1) % (currentSuggestions.length );
                    console.log(`Selected down ${selectedIdx}:`, currentSuggestions[selectedIdx]);
                }
                renderSuggestions(container, currentSuggestions, selectedIdx);
            },
            moveUp() {
                if (isShown) {
                    selectedIdx = ((selectedIdx == 0) ? currentSuggestions.length - 1 : selectedIdx - 1) % (currentSuggestions.length);
                    console.log(`Selected up ${selectedIdx}:`, currentSuggestions[selectedIdx]);
                }
                renderSuggestions(container, currentSuggestions, selectedIdx);
            },
            query(query) {
                selectedIdx = 0;
                console.log(`Query is ${query}`);
            },
            select() {
                isShown = false;
                container.innerHTML = '';
                container.setAttribute('style', getStyle());
                console.log(`Selected ${selectedIdx}:`, currentSuggestions[selectedIdx]);
                setTimeout(() => {
                    onSelectionCallback(currentSuggestions[selectedIdx]);
                }, 0);
            },
            isVisible(cb) {
                setTimeout(() => {
                    cb(isShown);
                }, 0);
            },
            onSelection(callback) {
                onSelectionCallback = callback;
            }
        };

        resolve(api);
    });
}