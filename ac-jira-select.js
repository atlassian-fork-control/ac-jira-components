import { define, emit, vdom, symbols } from 'skatejs';
import css from './ac-jira-select.css';
import AP from './ap';

function getInput(el) {
    return el[symbols.shadowRoot].querySelector(`.${css.locals.input}`);
}

function getValueHolder(el) {
    return el[symbols.shadowRoot].querySelector(`.${css.locals.value}`);
}

function getSuggestionsListPlacement(el) {
    let elBoundingBox = el.getBoundingClientRect();
    return [
        elBoundingBox.left, // x
        elBoundingBox.top + elBoundingBox.height, // y
        elBoundingBox.width // width
    ];
}

function showSuggestions(el, e) {
    el.suggestionsApi.showAt(...getSuggestionsListPlacement(el));
}

let AcJiraSelect = define('ac-jira-select', {
    events: {
        [`click .${css.locals.trigger}`] (el, e) {
            e.preventDefault();
            el.suggestionsApi.isVisible((isVisible) => {
                if (isVisible) {
                    el.suggestionsApi.hide();
                } else {
                    getInput(el).focus();
                    el.suggestionsApi.showAt(...getSuggestionsListPlacement(el));
                }
            });
        },
        [`click .${css.locals.input}`]: showSuggestions,
        [`focus .${css.locals.input}`]: showSuggestions,
        [`blur .${css.locals.input}`] (el, e) {
            window.setTimeout(() => {
                el.suggestionsApi.hide();
            }, 0);
        },
        [`keydown .${css.locals.input}`] (el, e) {
            if (e.which === 40) {
                el.suggestionsApi.isVisible((isVisible) => {
                    if (isVisible) {
                        el.suggestionsApi.moveDown();
                    } else {
                        el.suggestionsApi.showAt(...getSuggestionsListPlacement(el));
                    }
                });
            }
            else if (e.which === 38) {
                el.suggestionsApi.isVisible((isVisible) => {
                    if (isVisible) {
                        el.suggestionsApi.moveUp();
                    } else {
                        el.suggestionsApi.showAt(...getSuggestionsListPlacement(el));
                    }
                });
            }
            else if (e.which === 27) {
                el.suggestionsApi.hide();
            }
            else if (e.which === 13) {
                el.suggestionsApi.select();
            }
        },
        [`input .${css.locals.input}`] (el, e) {
            el.suggestionsApi.query(e.currentTarget.value);
            emit(el, 'input');
        },
        [`change .${css.locals.input}`] (el, e) {
            emit(el, 'change');
        }
    },
    attributes: {
        id(el, data) {
            if (el.id) {
                getInput(el).id = data.newValue;
            }
        },
        name(el, data) {
            el.querySelector('select').setAttribute('name', data.newValue);
        },

        placeholder(el, data) {
            getInput(el).setAttribute('placeholder', data.newValue);
        },
        value(el, data) {
            el.value = data;
        }
    },
    prototype: {
        get value() {
            return getValueHolder(this).value;
        },

        set value(value) {
            getValueHolder(this).value = "" + value;
            return this;
        },

        get label() {
            return getInput(this).value;
        },

        set label(value) {
            return getInput(this).value = "" + value;
        },

        blur: function () {
            getInput(this).blur();
            return this;
        },

        focus: function () {
            getInput(this).focus();
            return this;
        }
    },
    created(el) {
        el.style.pointerEvents = 'none';
        AP.jira.createSuggestionList((api) => {
            el.style.pointerEvents = 'auto';
            el.suggestionsApi = api;
            api.setSuggestions([
                {value:'aaa', label: 'display aaa'},
                {value:'bbb', label: 'display bbb'},
                {value:'ccc', label: 'display ccc'}
            ]);
            api.onSelected(function (selectedSuggestion) {
                el.value = selectedSuggestion.value;
                el.label = (selectedSuggestion.label) ? selectedSuggestion.label : selectedSuggestion.value;
            });
        });
    },
    render() {
        vdom.element('style', css.toString());
        vdom.element('input', {className: css.locals.input, type: 'text'});
        vdom.element('input', {className: css.locals.value, type: 'hidden'});
        vdom.element('button', {className: css.locals.trigger, tabindex: "-1"});
    }
});

export default AcJiraSelect;